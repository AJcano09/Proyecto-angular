﻿var app = angular.module('AppCamisetas', ['ngRoute']);

//Para que pueda reconocer el signo de '#'
app.config(['$locationProvider', function ($locationProvider) {
    $locationProvider.hashPrefix('');
}]);

app.config(function ($routeProvider) {
    $routeProvider 
     .when('/lista', {
            templateUrl: 'listaProductos.html',          
        })  
        .when('/verfacturas', {
            templateUrl: 'VerFacturas.html',          
        })   
       .when('/carrito', {
            templateUrl: 'App/Controller/Carrito.html',          
        })
       .when('/Targetas',{
        templateUrl: 'targetas.html',
       })
          .when('/verProductos', {
            templateUrl: 'App/Controller/Carrito.html',          
        })
        .when('/Productos', {
            templateUrl: 'productos.html',          
        })
        .when('/categorias', {
           templateUrl: 'categorias.html',
        })
        .when('/verCarrito', {
           templateUrl: 'App/Controller/verCarrito.html',
        })
        .when('/Marcas', {
           templateUrl: 'Marcas.html'
        });
});

app.constant('CONFIG', {
     //API_URL: 'https://localhost:44345/'    
    API_URL:'http://localhost:2476/'
});

function Mensaje(Titulo, Mensaje, Icon) {
    sweetAlert(Titulo, Mensaje, Icon);
}
   function notify(div,titulo,mensaje) {
       // body...
       $.notify(div,titulo,mensaje);
   }
