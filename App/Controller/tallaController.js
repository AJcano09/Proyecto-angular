app.controller('tallaController', ['dataService', '$scope', 'CONFIG',
    function (dataService, $scope, CONFIG) {
        $scope.talla = null;

        dataService.getData(CONFIG.API_URL + 'api/talla/1').then(function (result) {
            if (result.status == 200)
            {
            
                $scope.talla = result.data;
            }

        })
    }
])