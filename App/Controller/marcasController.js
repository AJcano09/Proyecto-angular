﻿app.controller('marcasController', ['dataService', '$scope', 'CONFIG',
    function (dataservice, $scope, CONFIG) {
        $scope.marcas = null;

        dataservice.getData(CONFIG.API_URL + 'api/marcas').then(function (result) {
            if (result.status == 200)
            {
                $scope.marcas = result.data;
            }

        })
    }
])