app.controller("facturaController",['dataService','$scope','CONFIG',function(dataService,$scope,CONFIG){
$scope.Articulo=null;
$scope.res=null;
$scope.producto=null;
$scope.Total=null;
$scope.fecha= new Date();
$scope.numero_Fac=null; 
get();
    getTotal();
function get() {
    debugger;
        dataService.getData(CONFIG.API_URL + 'api/Factura/1')
        .then(function (result) {
            if (result.status == 200) {          
                $scope.Articulo = result.data;
			
            $scope.visible=false;
          
            }
            else
            {
                debugger;
                Mensaje("Error","Error al Cargar los dartos","error");
            }
        })

      
        
    }

$scope.Quitar=function(elemento){

     swal({
                title: "Desea Eliminar?",
                text: "Usted Esta Seguro que desea Quitar el Producto del Carrito!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, Eliminar!",
                cancelButtonText: "No, Graicas!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm, id) {
                    id = elemento;
                    if (isConfirm) {
                       debugger;
dataService.DeleteData(CONFIG.API_URL+'api/Factura/'+id).then(function(resultado){
if(resultado.status==200)
{
Mensaje("Quitado!!","Producto Eliminado del Carrito","info");

get();

}
});

                    } else {
                        Mensaje("Cancelado", "La Accion fue Cancelada:)", "error");
                    }
                });

}


function getTotal()
{
	dataService.postData(CONFIG.API_URL+'api/TotalRegistos').then(function(resultado){
	if(resultado.status==200)
	{
                    debugger;
		$scope.Total=resultado.data;
	}

})
}

$scope.addProducto=function(producto){
    	debugger;
    	var cantidad=producto.cantidad;
    	var Tala=producto.idTalla;
    if(cantidad !="" && Tala!="" && Tala!=0 && cantidad!="undefined" && Tala!="undefined" && !isNaN(cantidad) && cantidad>0)
    {
    		dataService.postData(CONFIG.API_URL+'api/Factura', producto).then(function (resultado){
    		if(resultado.status==200)
    		{

    			Mensaje("Agregado!!!","Producto Agregado al Carrito","info");

    			get();
                getTotal();
      		}
    		else
    		{
    			Mensaje("Error","No se ha Encontrado el Producto","error");
    		}
    	});

    }
    else
    {
    	Mensaje("Error de Validacion","Ha Ocurrido un error complete los campos, o los datos no son validos","warning");
    }

    }
 

    $scope.Factura=function()
    {
    	var datosFactura={
    		fecha:$scope.fecha,
    		numero_Fac:$scope.numero_Fac,
    	};
    	debugger;
    if (datosFactura!=null) {

    		dataService.postData(CONFIG.API_URL+'api/FacturarVenta',datosFactura).then(function(resultado) {
    
    			if(resultado.data==true)
    			{
    				debugger;
    		
    			get();
    			Mensaje("Exito!!!","Usted! ha realizado su Compra con Exito","success");

    			}
                else{
                  Mensaje("Error!!!","No hay datos en el Carrito","error");  
                }
    	
    	})
    }
    else
    {
    	Mensaje("Error","Noy hay Datos en el Carrito","error")
    }
    }

    $scope.obtener=function(id){

dataService.getDataById(CONFIG.API_URL+'api/Factura/obtener/'+id).then(function(resultado){
if(resultado.status==200)
{
    $scope.cantidad=resultado.data[0].cantidad;
    $scope.idProducto=resultado.data[0].idProducto;
    $scope.visible=true;
  
}
})  
}
    $scope.cancel=function()
    {
    $scope.visible=false;
    
}
$scope.update=function(){

    var obj={
        cantidad:$scope.cantidad,
        idProducto:$scope.idProducto

    };
if(obj.cantidad>0)
{
dataService.putData(CONFIG.API_URL+'api/Factura',obj).then(function(resultado){

            if(resultado.data==true)
            {
                $scope.visible=false;
                    get();
            }

    });
}
else
{
    Mensaje("Informacion","La Cantidad de Ser mayor a cero","info");
}

    
}


}])