﻿app.controller('productosController', ['dataService', '$scope', 'CONFIG',
    function (dataService, $scope, CONFIG) {
        $scope.alertMessage = null;

        // Función que se encargará de mostrar el mensaje de alerta.
        function showAlert() {
            $scope.alertMessage = {
                // 'type' define el aspecto que tendrá el mensaje de alerta.
                type: "success",
                text: "Texto del mensaje a mostrar",
                // Si 'closable' es 'true' se mostrará un botón para ocultar de manera manual el mensaje.
                closable: true,
                // número de segundos antes de que el mensaje de alerta desaparezca de forma automática.
                delay: 3
            }
        };
        //Contendra los productos que envie la api
        $scope.Productos = null;
        $scope.prod = null;
        //Envio de Productos
        $scope.AllProducto = null;
        //Obtendra el Producto por ID
        $scope.ProductosbyId = null;
        //Contendra  el total de productos
        $scope.Total = 0;
        //Objeto de Cat
        $scope.Cat = null;

        //Mandar a llamar los productos
        get();
        function get() {
            dataService.getData(CONFIG.API_URL + 'api/productos/1').then(function (result) {
                if (result.status == 200) {
                    $scope.Total = Math.round(result.data[0].totalProductos / 8);
                    $scope.Productos = result.data;
                    console.log(result);
                    $scope.visible = false;
                }
                else
                {
                    Mensaje("Error","El Servicio no esta Disponible en Estos Momentos","Error");
                }
            });
        }
        $scope.Detalle=function(argument) {
            debugger;
          dataService.getDataById(CONFIG.API_URL + 'api/productos/obtener/' + argument).then(function (result) {
                if (result.status == 200) {
                    $scope.ProductosbyId = result.data;                  

                }
            })
        }
        $scope.Obtener = function (id) {

            dataService.getDataById(CONFIG.API_URL + 'api/productos/obtener/' + id).then(function (result) {
                if (result.status == 200) {
                    $scope.ProductosbyId = result.data;
                    $scope.IdProducto = result.data[0].idProducto;
                    $scope.Nombre = result.data[0].nombre;
                    $scope.Marca = result.data[0].marca;
                    $scope.Categoria = result.data[0].categoria;
                    $scope.Precio = result.data[0].precio;
                    $scope.UrlImagen = result.data[0].urlImagen;
                    $scope.Descripcion = result.data[0].descripción;
                    $scope.Iva = result.data[0].iva;
                    $scope.IdCategoria = result.data[0].idCategoria;
                    $scope.IdMarca = result.data[0].idMarca;
                    $scope.Estado = result.data[0].estado;

                    $scope.visible = true;
                }
            })
        }

        //Metodo de Paginacion
        $scope.Paginar = function (pagina) {
            dataService.getData(CONFIG.API_URL + 'api/productos/' + pagina).then(function (result) {
                if (result.status == 200) {
                    $scope.Total = Math.round(result.data[0].totalProductos / 8);
                    $scope.Productos = result.data;
                }
            });
        }
        //Metodo Eliminar
        $scope.Delete = function (elemento) {
            swal({
                title: "Desea Eliminar?",
                text: "Usted Esta Seguro que desea Eliminar el Elemento!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, Eliminar!",
                cancelButtonText: "No, Graicas!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm, id) {
                    id = elemento;
                    if (isConfirm) {
                        dataService.DeleteData(CONFIG.API_URL + "api/productos/" + id).then(function (result) {
                            if (result.status == 200) {
                                get();
                                Mensaje("Exito", "Datos Elimininados con Exito", "success");
                            }
                        });

                    } else {
                        Mensaje("Cancelado", "La Accion fue Cancelada:)", "error");
                    }
                });

        }

        //Metodo que guarda las categorias
        $scope.GuardarCat = function () {
            dataService.postData(CONFIG.API_URL + 'api/categorias', $scope.Cat).then(function (result) {
                if (result.status == 201) {

                }
            });
        }
        //Metodo que guarda las Productos
        $scope.GuardarProd = function () {
            dataService.postData(CONFIG.API_URL + 'api/productos', $scope.AllProducto).then(function (result) {
                if (result.status == 201) {

                    get();
           
                    Mensaje("Exito", "Datos Guardados con Exito", "success");
                }
            });
        }
        //Metodo que guarda las Productos

        $scope.UpdateProd = function () {
            var ProductoData = {
                IdProducto: $scope.IdProducto,
                Nombre: $scope.Nombre,
                Marca: $scope.Marca,
                Categoria: $scope.Categoria,
                Precio: $scope.Precio,
                UrlImagen: $scope.UrlImagen,
                Descripcion: $scope.Descripcion,
                Iva: $scope.Iva,
                IdCategoria: $scope.IdCategoria,
                IdMarca: $scope.IdMarca,
                Estado: $scope.Estado

            };
            dataService.putData(CONFIG.API_URL + 'api/productos', ProductoData).then(function (result) {
                if (result.status == 200) {
                    get();
                    debugger;

                    Mensaje("Exito", "Datos Modificacdos con Exito", "success");
                }
            });


        }
        $scope.ocultar = function () { $scope.visible = false; }
    }]);
