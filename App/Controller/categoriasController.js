﻿app.controller('categoriasController', ['dataService', '$scope', 'CONFIG',

    function (dataservice, $scope, CONFIG) {
        $scope.Categoria = null;

        dataservice.getData(CONFIG.API_URL + 'api/categorias').then(function (result) {
            if (result.status == 200)
            {
                $scope.Categoria = result.data;
            }
        });
    }

])