app.controller('paisController', ['dataService', '$scope', 'CONFIG',
    function (dataservice, $scope, CONFIG) {
        $scope.pais = null;

        dataservice.getData(CONFIG.API_URL + 'api/Pais').then(function (result) {
            if (result.status == 200)
            {
                $scope.pais = result.data;
            }

        })
    }
])