﻿app.service('dataService', ['$http', function ($http) {
    delete $http.defaults.headers.common['X-Requested-With'];

    //Obtiene los datos
    this.getData = function (url) {
        return $http({
            method: 'GET',
            url:url
        });
    }
    //Obtiene los datos x Id
    this.getDataById = function (url, id) {
       
        return $http({
            method: 'GET',
            url: url
        });
    }
    //Envia Datos
    this.postData = function (url,datos) {
        
        return $http({
            method: 'POST',
            url: url,
            data:datos
        });
    }
    //Update Datos
    this.putData = function (url, datos) {
        
        return $http({
            method: 'PUT',
            url: url,
            data: datos
        });
    }
    //Delte Datos
    this.DeleteData = function (url, id) {
        debugger;
        return $http({
            method: 'Delete',
            url: url,
            data: id
        });
    }
}]);